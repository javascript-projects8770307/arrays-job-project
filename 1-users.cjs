const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/


const userArray = Object.keys(users);

// Q1 Find all users who are interested in playing video games.
let interestedInVideoGames = userArray.filter(user => {
    let userInterest = users[user].interests[0].includes('Video Games');
    return userInterest;
})
console.log("Users interested in playing Video Games: ", interestedInVideoGames);


// Q2 Find all users staying in Germany.
let userInGermany = userArray.filter(user => {
    let userNationality = users[user].nationality.includes('Germany');
    return userNationality;
})
console.log("Users Staying in Germany: ", userInGermany);



// Q3 Sort users based on their seniority level 

let seniorUser = userArray.sort((first, second) => {
    let firstUserDesgination = users[first].desgination;
    let secondUserDesgination = users[second].desgination;
    let firstUserAge = users[first].age;
    let secondUserAge = users[second].age;

    if (firstUserDesgination.includes('Senior') && secondUserDesgination.includes('Intern')
        || firstUserAge > secondUserAge) {
        return -1;
    } else if (firstUserDesgination.includes('Intern') && secondUserDesgination.includes('Senior')
        || firstUserAge < secondUserAge) {
        return 1;
    }
    else {
        return 0;
    }

})
console.log("seniors",seniorUser);


// Q4 Find all users with masters Degree.
let usersWithMasterDegree = userArray.filter(user => {
    let userWithMaster = users[user].qualification.includes('Masters');
    return userWithMaster;
})
console.log("Users with Master Degree: ", usersWithMasterDegree);



// Q5 Group users based on their Programming language mentioned in their designation.
let programming = userArray.reduce((groups, user) => {
    let designationArray = users[user].desgination.split(' ');
    if(designationArray[0] === "Senior"){
        designationArray = designationArray[1]
    }
    else if(designationArray[0] === "Intern"){
        designationArray = designationArray[2]
    }
    else{
        designationArray = designationArray[0]
    }
    let languageType = (groups[designationArray] || []);
    
    languageType.push(user);
    groups[designationArray] = languageType;
    return groups;

}, {})

console.log(programming);
