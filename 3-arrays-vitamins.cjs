const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/

// 1. Get all items that are available 
let allItemsAvailable = (itemsData) => {
    let itemsAvailable = itemsData.filter(item => {
        return item.available;
    })
    return itemsAvailable;
};
console.log(allItemsAvailable(items));


// 2. Get all items containing only Vitamin C.
let itemWithVitaminC = (itemsData) => {
    let itemsVitaminC = itemsData.filter(item => {
        let vitamin = "Vitamin C"
        if (item.contains === vitamin) {
            return item.contains;
        } else {
            return false;
        }
    });
    return itemsVitaminC;
}
console.log(itemWithVitaminC(items));


// 3. Get all items containing Vitamin A.
let itemWithVitaminA = (itemsData) => {
    let itemsVitaminA = itemsData.filter(item => {
        return item.contains.includes("A");
    });
    return itemsVitaminA;
}
console.log(itemWithVitaminA(items));


// 4. Group items based on the Vitamins that they contain in the following format:
let groupBasedOnVitamins = (itemsData) => {
    const vitaminGroup = {};

    itemsData.filter(item => {
        if (!vitaminGroup[item.contains]) {
            vitaminGroup[item.contains] = [item.name];
        } else {
            vitamin[item.contains].push(item.name);
        }
    })
    return vitaminGroup;
}
console.log(groupBasedOnVitamins(items));

// 5. Sort items based on number of Vitamins they contain.
let sorted = items.sort((first, second) => {
    if (first.contains.length > second.contains.length) {
        return -1;
    } else if (first.contains.length < second.contains.length) {
        return 1;
    } else {
        return 0;
    }
})
console.log(sorted);

