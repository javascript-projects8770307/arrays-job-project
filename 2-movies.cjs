const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/

const movies = Object.keys(favouritesMovies);

// Q1. Find all the movies with total earnings more than $500M. 
let movieEarning500 = movies.filter(movie => {
    earning = parseInt(favouritesMovies[movie].totalEarnings.slice(1, -1));
    if (earning > 500) {
        return movie;
    }
})
console.log(movieEarning500);

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
let movieOscarNomination = movies.filter(movie => {
    earning = parseInt(favouritesMovies[movie].totalEarnings.slice(1, -1));
    if (favouritesMovies[movie].oscarNominations > 3 && earning > 500) {
        return movie;
    }
})
console.log(movieOscarNomination);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".
let leonardoMovies = movies.filter(movie => {
    return favouritesMovies[movie].actors
        .includes('Leonardo Dicaprio');
})
console.log(leonardoMovies);

// Q.4 Sort movies (based on IMDB rating)
let sorted = movies.sort((first, second) => {
    let firstImdb = favouritesMovies[first].imdbRating;
    let secondImdb = favouritesMovies[second].imdbRating;
    let firstEarning = parseInt(favouritesMovies[first].totalEarnings.slice(1, -1));
    let secondEarning = parseInt(favouritesMovies[second].totalEarnings.slice(1, -1));
    return secondImdb - firstImdb || secondEarning - firstEarning;
})
    .map(movie => {
        let movieObject = {};
        movieObject[movie] = favouritesMovies[movie];
        return movieObject;
    })
console.log(sorted);

let movieGroups = movies.reduce((groups, movie) => {
    let generes = favouritesMovies[movie].genre;
    if (generes.includes('drama')) {
        let dramaCategory = groups['drama'] || [];
        dramaCategory.push(movie);
        groups['drama'] = dramaCategory;
    } else if (generes.includes('sci-fi')) {
        let scifiategory = groups['sci-fi'] || [];
        scifiategory.push(movie);
        groups['sci-fi'] = scifiategory;
    } else if (generes.includes('adventure')) {
        let adventureCategory = groups['adventure'] || [];;
        adventureCategory.push(movie);
        groups['adventure'] = adventureCategory;
    } else if (generes.includes('thriller')) {
        let thrillerCategory = groups['thriller'] || [];
        thrillerCategory.push(movie);
        groups['thriller'] = thrillerCategory;
    } else if (generes.includes('crime')) {
        let crimeCategory = groups['crime'] || [];
        crimeCategory.push(movie);
        groups['crime'] = crimeCategory;
    }

    return groups;
}, {})
console.log(movieGroups);