const arrayData = [{ "id": 1, "first_name": "Valera", "last_name": "Pinsent", "email": "vpinsent0@google.co.jp", "gender": "Male", "ip_address": "253.171.63.171" },
{ "id": 2, "first_name": "Kenneth", "last_name": "Hinemoor", "email": "khinemoor1@yellowbook.com", "gender": "Polygender", "ip_address": "50.231.58.150" },
{ "id": 3, "first_name": "Roman", "last_name": "Sedcole", "email": "rsedcole2@addtoany.com", "gender": "Genderqueer", "ip_address": "236.52.184.83" },
{ "id": 4, "first_name": "Lind", "last_name": "Ladyman", "email": "lladyman3@wordpress.org", "gender": "Male", "ip_address": "118.12.213.144" },
{ "id": 5, "first_name": "Jocelyne", "last_name": "Casse", "email": "jcasse4@ehow.com", "gender": "Agender", "ip_address": "176.202.254.113" },
{ "id": 6, "first_name": "Stafford", "last_name": "Dandy", "email": "sdandy5@exblog.jp", "gender": "Female", "ip_address": "111.139.161.143" },
{ "id": 7, "first_name": "Jeramey", "last_name": "Sweetsur", "email": "jsweetsur6@youtube.com", "gender": "Genderqueer", "ip_address": "196.247.246.106" },
{ "id": 8, "first_name": "Anna-diane", "last_name": "Wingar", "email": "awingar7@auda.org.au", "gender": "Agender", "ip_address": "148.229.65.98" },
{ "id": 9, "first_name": "Cherianne", "last_name": "Rantoul", "email": "crantoul8@craigslist.org", "gender": "Genderfluid", "ip_address": "141.40.134.234" },
{ "id": 10, "first_name": "Nico", "last_name": "Dunstall", "email": "ndunstall9@technorati.com", "gender": "Female", "ip_address": "37.12.213.144" }]

/* 
    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file
*/

//1. Find all people who are Agender
let agenderPeople = (arrayData) => {
    let agendger = arrayData.filter(person => {
        return person.gender.includes("Agender");
    });
    return agendger;
};
// console.log(agenderPeople(arrayData));
// let agender = (agenderPeople(arrayData));


// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
let ipAddress = (agenderArray) => {
    agenderArray.forEach(element => {
        let ipArray = element.ip_address.split('.');
        let result = ipArray.map(Number);
        

        element.ip_address = result;
    });
};
// ipAddress(arrayData);
// console.log(arrayData);

// 3. Find the sum of all the second components of the ip addresses.
let sumOfSecondComponent = (arrayData) => {
    let sumArray = [];

    arrayData.forEach(element => {
        sumArray.push(element.ip_address[1]);
    })
    let result = sumArray.reduce((sum, item) => {
        return sum + item;
    }, 0);

    return result;
};
// console.log('Sum of Second Component: ',sumOfSecondComponent(arrayData));

// 3. Find the sum of all the fourth components of the ip addresses.
let sumOfFourthComponent = (arrayData) => {
    let sumArray = [];

    arrayData.forEach(element => {
        sumArray.push(element.ip_address[3]);
    })
    let result = sumArray.reduce((sum, item) => {
        return sum + item;
    }, 0);

    return result;
};

// console.log('Sum of Fourth Component: ',sumOfFourthComponent(arrayData));

// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
let fullName = (arrayData) => {

    arrayData.forEach(element => {
        element.full_name = element.first_name + ' ' + element.last_name;
    })
};
// fullName(arrayData);
// console.log(arrayData);

// 5. Filter out all the .org emails
let allOrgEmails = (arrayData) => {
    let orgEmails = arrayData.filter(person => {
        return person.email.endsWith(".org");
    });
    return orgEmails;
};
// console.log(allOrgEmails(arrayData));



// 6. Calculate how many .org, .au, .com emails are there
let calculateEmails = (arrayData) => {
    const emailCount = {};

    let orgEmails = arrayData.filter(person => {
        return person.email.endsWith('.org');
    });
    let auEmails = arrayData.filter(person => {
        return person.email.endsWith(".au");
    });
    let comEmails = arrayData.filter(person => {
        return person.email.endsWith(".com");
    });

    emailCount.org_emails = orgEmails.length;
    emailCount.au_emails = auEmails.length;
    emailCount.com_emails = comEmails.length;

    return emailCount;

};
// console.log(calculateEmails(arrayData));


// 7. Sort the data in descending order of first name
let sortDataWithFirstName = (arrayData) => {
    arrayData.sort((firstPerson, secondPerson) => {
        let fPerson = firstPerson.first_name.toLowerCase();
        let sPerson = secondPerson.first_name.toLowerCase();

        if (fPerson < sPerson) {
            return -1;
        }
        if (fPerson > sPerson) {
            return 1;
        }
        return 0;
    })

    let sortedData = [];
    arrayData.forEach(element => {
        sortedData.push(element);
    })
    return sortedData;

};
// console.log(sortDataWithFirstName(arrayData));