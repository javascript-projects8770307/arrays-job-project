const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/
// Q1. Find all the items with price more than $65.
let allPrices = products.map(items => {
    let product = Object.keys(items);

    return product.map(item => {
        if (Array.isArray(items[item])) {

            return items[item].filter(utensil => {
                let uten = Object.keys(utensil);
                let utenObj = {};
                utenPrice = parseInt(utensil[uten].price.substring(1));

                if (utenPrice > 65) {
                    utenObj[uten] = utensil[uten].price;
                    return utenObj;
                }
            })
                .filter(function (element) {
                    return element !== undefined;
                });

        } else {
            let itemObj = {};
            let itemPrice = parseInt(items[item].price.substring(1));

            if (itemPrice > 65) {
                itemObj[item] = itemPrice;
                return itemObj;
            }

        }
    }).filter(function (element) {
        return element !== undefined;
    });
})
console.log(JSON.stringify(allPrices));

// Q2. Find all the items where quantity ordered is more than 1.

let quantities = products.map(items => {
    let product = Object.keys(items);

    return product.map(item => {
        if (Array.isArray(items[item])) {

            return items[item].filter(utensil => {
                let uten = Object.keys(utensil);
                let utenObj = {};
                utenQuantity = utensil[uten].quantity;

                if (utenQuantity > 1) {
                    utenObj[uten] = utenQuantity;
                    return utenObj;
                }
            })
                .filter(function (element) {
                    return element !== undefined;
                });

        } else {
            let itemObj = {};
            let itemQuantity = items[item].quantity;

            if (itemQuantity > 1) {
                itemObj[item] = itemQuantity;
                return itemObj;
            }

        }
    }).filter(function (element) {
        return element !== undefined;
    });
})

console.log(JSON.stringify(quantities));

// Q.3 Get all items which are mentioned as fragile.
let fragileItems = products.map(items => {
    let product = Object.keys(items);

    return product.map(item => {
        if (Array.isArray(items[item])) {

            return items[item].filter(utensil => {
                let uten = Object.keys(utensil);
                let utenObj = {};
                utenType = utensil[uten].type;

                if (utenType === 'fragile') {
                    utenObj[uten] = utenType;
                    return utenObj;
                }
            })
                .filter(function (element) {
                    return element !== undefined;
                });

        } else {
            let itemObj = {};
            let itemType = items[item].type;

            if (itemType === 'fragile') {
                itemObj[item] = itemType;
                return itemObj;
            }

        }
    }).filter(function (element) {
        return element !== undefined;
    });
})

console.log(JSON.stringify(fragileItems))


// Q.4 Find the least and the most expensive item for a single quantity.
let leastPrice = products.map(items => {
    let product = Object.keys(items);

    return product.map(item => {
        if (Array.isArray(items[item])) {

            return items[item].filter(utensil => {
                let uten = Object.keys(utensil);
                let utenObj = {};
                utenQuantity = utensil[uten].quantity;
                utenPrice = utensil[uten].price;

                if (utenQuantity === 1) {
                    utenObj[uten] = utenPrice;
                    return utenObj;
                }
            })
                .filter(function (element) {
                    return element !== undefined;
                });

        } else {
            let itemObj = {};
            let itemQuantity = items[item].quantity;
            let itemPrice = items[item].price;

            if (itemQuantity === 1) {
                itemObj[item] = itemPrice;
                return itemObj;
            }

        }
    }).filter(function (element) {
        return element !== undefined;
    });
})
console.log(JSON.stringify(leastPrice));
